import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/noteslist.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    TextEditingController titleController = TextEditingController();
    TextEditingController dateController = TextEditingController();
    TextEditingController notesController = TextEditingController();

    addNotes(){
      final title = titleController.text;
      final date = dateController.text;
      final notes = notesController.text;

      final ref = FirebaseFirestore.instance.collection("notes").doc();
      
      return ref
          .set({"title":title,"data":date,"notes": notes,"doc_id":ref.id});

    }


    return Column(
      children: [
        Column(
              children : [
                Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 5),
                child: TextField(
                  controller: titleController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20)),
                    hintText: 'input title'
                  ),),),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 5),
                  child: TextField(
                    controller: dateController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20)),
                    hintText: 'input date'
                    ),),),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 5),
                  child: TextField(
                    controller: notesController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20)),
                    hintText: 'input notes'
                    ),),),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 10),
                  child: ElevatedButton(
                      onPressed: (){addNotes();},
                      child: const Text('add')),
                )
            ],
            ),
        NotesList()
      ],
    );
  }

}