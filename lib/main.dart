import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_app/homepage.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyAvC3vlJ3ycYgrNaiSVJ6-FjApdF2-vD5w",
          authDomain: "myprojects-965c0.firebaseapp.com",
          projectId: "myprojects-965c0",
          storageBucket: "myprojects-965c0.appspot.com",
          messagingSenderId: "179826015527",
          appId: "1:179826015527:web:b4e89ddcce887a1a1f3d02",
          measurementId: "G-F7CV3HM0DJ"
      ));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp( home : MyHomePage(title: "Save Notes"));
  }
}
class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),

      body: const HomePage()
    );
  }
}

