import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class NotesList extends StatefulWidget {
  const NotesList({Key? key}) : super(key: key);

  @override
  State<NotesList> createState() => _NotesList();
}
  class _NotesList extends State<NotesList> {

    final Stream<QuerySnapshot> streams = FirebaseFirestore.instance.collection("notes")
        .snapshots();

  @override
  Widget build(BuildContext context) {

    TextEditingController _titleController = TextEditingController();
    TextEditingController _dateController = TextEditingController();
    TextEditingController _notesController = TextEditingController();

    void _delete(docId){
      FirebaseFirestore.instance.collection("notes")
          .doc(docId)
          .delete();
    }

    void _update(data){

      var collection = FirebaseFirestore.instance.collection("notes");
      _titleController.text = data['title'];
      _dateController.text = data['date'];
      _notesController.text = data['notes'];

      showDialog(
          context: context,
          builder:(_) => AlertDialog(
            title: const Text("Update"),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextField(
                  controller: _titleController,
                ),
                TextField(
                  controller: _dateController,
                ),
                TextField(
                  controller: _notesController,
                ),
                TextButton(onPressed: (){
                  collection.doc(data['doc_id'])
                      .update({
                    "title": _titleController.text,
                    "date": _dateController.text,
                    "notes": _notesController.text
                  });
                  Navigator.pop(context);
                }, child: const Text("update"))
              ],
            ),
          )
      );
    }
    return StreamBuilder(
      stream: streams,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return const Text("Something went wrong");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }
        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                child: SizedBox(
                  height: (MediaQuery.of(context).size.height),
                  width : (MediaQuery.of(context).size.width),
                  child: ListView(
                    children: snapshot.data!.docs.map((DocumentSnapshot docSnapshot) {
                      Map<String, dynamic> data = docSnapshot.data()! as Map<String, dynamic>;
                      return Column(
                        children: [
                          Card(
                            child: Column(
                              children: [
                                ListTile(
                                  title: Text(data['title']+"         "+data['date']),
                                  subtitle: Text(data['notes']),
                                ),
                                ButtonTheme(
                                    child: ButtonBar(
                                      children: [
                                        OutlineButton.icon(
                                          onPressed: (){_update(data);
                                          },
                                          icon: const Icon(Icons.edit),
                                          label: const Text("edit"),
                                        ),
                                        OutlineButton.icon(
                                          onPressed: (){_delete(data['doc_id']);
                                          },
                                          icon: const Icon(Icons.delete),
                                          label: const Text("delete"),
                                        )
                                      ],
                                    ))
                              ],
                            ),
                          ),
                        ],
                      );
                    }).toList(),

                  )))
            ],
          );
        }else {
          return const Text("No data");
        }
      }
    );
  }
}